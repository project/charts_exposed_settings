CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Maintainers


INTRODUCTION
------------

This module enables users to populate Views-based chart settings via exposed
fields or filters added to the View.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/charts_exposed_settings

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/search/charts_exposed_settings


REQUIREMENTS
------------

Charts must be installed.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


HOW IT WORKS
------------

After you have created a Chart in Views, you can add exposed filters or fields
to the View. These exposed filters or fields will be used to populate any
of the following settings in the Chart:
* Title
* Subtitle
* X-axis label
* Y-axis label

MAINTAINERS
-----------

Current maintainers:
* Daniel Cothran (andileco) - https://www.drupal.org/u/andileco
