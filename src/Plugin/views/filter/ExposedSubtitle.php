<?php

namespace Drupal\charts_exposed_settings\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * @file
 * Defines Drupal\charts_exposed_settings\Plugin\views\filter\ExposedSubtitle.
 */

/**
 * Filter to expose subtitle input.
 *
 * @ingroup views_filter_handlers
 * @ViewsFilter("field_exposed_subtitle")
 */
class ExposedSubtitle extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isExposed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state): void {
    $form['chart_subtitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subtitle'),
      '#description' => $this->t('Enter the subtitle you would like to appear in your chart.'),
      '#default_value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['exposed'] = ['default' => TRUE];
    $options['chart_subtitle'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // Hide the operator options.
    $form['operator']['#access'] = FALSE;
    // Hide the value options.
    $form['value']['#access'] = FALSE;
    // Set the identifier to the field name.
    $form['expose']['identifier']['#default_value'] = 'chart_subtitle';

    return $form;
  }

}
