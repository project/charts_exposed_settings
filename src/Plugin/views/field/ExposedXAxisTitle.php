<?php

namespace Drupal\charts_exposed_settings\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @file
 * Defines Drupal\charts_exposed_settings\Plugin\views\field\ExposedXAxisTitle.
 */

/**
 * Field to expose x-axis title input.
 *
 * @ingroup views_field_handlers
 * @ViewsField("field_exposed_xaxis_title")
 */
class ExposedXAxisTitle extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isExposed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state): void {
    $form['x_axis_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X-Axis Title'),
      '#description' => $this->t('Enter the x-axis title you would like to appear in your chart.'),
      '#default_value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['x_axis_title'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
  }

}
