<?php

namespace Drupal\charts_exposed_settings\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @file
 * Defines Drupal\charts_exposed_settings\Plugin\views\field\ExposedYAxisTitle.
 */

/**
 * Field to expose y-axis title input.
 *
 * @ingroup views_field_handlers
 * @ViewsField("field_exposed_yaxis_title")
 */
class ExposedYAxisTitle extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isExposed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state): void {
    $form['y_axis_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Y-Axis Title'),
      '#description' => $this->t('Enter the y-axis title you would like to appear in your chart.'),
      '#default_value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['y_axis_title'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
  }

}
